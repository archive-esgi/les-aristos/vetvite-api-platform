<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\User;
use App\Repository\CareRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CareRepository::class)
 * @ApiResource(
 *     mercure=true,
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_PRO')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object.getPro() == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object.getPro() == user"}
 *     },
 *     normalizationContext={"groups"={"care:read"}, "enable_max_depth"="true"},
 *     denormalizationContext={"groups"={"care:write"}, "enable_max_depth"="true"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"type": "exact", "pro": "exact", "office": "exact"})
 */
class Care
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"care:read","appointment:read","office:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotNull
     * @Assert\Positive
     * @Groups({"care:read","care:write","appointment:read","office:read"})
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="cares")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"care:read","care:write","appointment:read","office:read"})
     * @MaxDepth(1)
     */
    private $pro;

    /**
     * @ORM\ManyToOne(targetEntity=Office::class, inversedBy="cares")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"care:read","care:write","appointment:read"})
     * @MaxDepth(1)
     */
    private $office;

    /**
     * @ORM\Column(type="string", length=35)
     * @Assert\NotNull
     * @Groups({"care:read","care:write","appointment:read","office:read"})
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Appointment::class, mappedBy="care", orphanRemoval=true)
     * @Groups({"care:read","care:write","office:read"})
     * @MaxDepth(1)
     */
    private $appointments;

    public function __construct()
    {
        $this->appointments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPro(): ?User
    {
        return $this->pro;
    }

    public function setPro(?User $pro): self
    {
        $this->pro = $pro;

        return $this;
    }

    public function getOffice(): ?Office
    {
        return $this->office;
    }

    public function setOffice(?Office $office): self
    {
        $this->office = $office;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Appointment[]
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(Appointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments[] = $appointment;
            $appointment->setCare($this);
        }

        return $this;
    }

    public function removeAppointment(Appointment $appointment): self
    {
        if ($this->appointments->removeElement($appointment)) {
            // set the owning side to null (unless already changed)
            if ($appointment->getCare() === $this) {
                $appointment->setCare(null);
            }
        }

        return $this;
    }
}
