<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\User;
use App\Repository\OfficeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OfficeRepository::class)
 * @ApiResource(
 *     mercure=true,
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_PRO') and user.getEnabled() == 1"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object.getCreator() == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object.getCreator() == user"},
 *     },
 *     normalizationContext={"groups"={"office:read"}, "enable_max_depth"="true"},
 *     denormalizationContext={"groups"={"office:write"}, "enable_max_depth"="true"}
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "name": "partial",
 *     "address": "partial",
 *     "postalCode": "start",
 *     "city": "partial",
 *     "country": "partial",
 *     "phoneNumber": "exact",
 *     "faxNumber": "exact",
 *     "pros": "exact",
 *     "creator": "exact",
 * })
 */
class Office
{
    public const SET_ENABLED = 1;
    public const SET_WAITING = 0;
    public const SET_ARCHIVED = -1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank
     * @Assert\Length(min=3, max=64)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max=64)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=76)
     * @Assert\NotBlank
     * @Assert\Length(min=5, max=76)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=14)
     * @Assert\NotBlank
     * @Assert\Length(max=14)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank
     * @Assert\Length(max=32)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank
     * @Assert\Length(max=32)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=24)
     * @Assert\NotBlank
     * @Assert\Length(max=24)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     * @Assert\Length(max=24)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $faxNumber;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Email
     * @Assert\Length(min=5, max=255)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(max=510)
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotNull
     * @Assert\Choice({-1, 0, 1})
     * @Groups({"office:read","office:write","appointment:read","care:read","user:read"})
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="offices")
     * @MaxDepth(2)
     * @Groups({"office:read","office:write"})
     */
    private $pros;

    /**
     * @ORM\OneToMany(targetEntity=Care::class, mappedBy="office")
     * @MaxDepth(1)
     * @Groups({"office:read","office:write"})
     */
    private $cares;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="createdOffices")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     * @Groups({"office:read", "office:write"})
     */
    private $creator;

    public function __construct()
    {
        $this->pros = new ArrayCollection();
        $this->cares = new ArrayCollection();
        $this->setStatus(self::SET_WAITING);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getPros(): Collection
    {
        return $this->pros;
    }

    public function addPro(User $pro): self
    {
        if (!$this->pros->contains($pro)) {
            $this->pros[] = $pro;
            $pro->addOffice($this);
        }

        return $this;
    }

    public function removePro(User $pro): self
    {
        if ($this->pros->removeElement($pro)) {
            $pro->removeOffice($this);
        }

        return $this;
    }

    /**
     * @return Collection|Care[]
     */
    public function getCares(): Collection
    {
        return $this->cares;
    }

    public function addCare(Care $care): self
    {
        if (!$this->cares->contains($care)) {
            $this->cares[] = $care;
            $care->setOffice($this);
        }

        return $this;
    }

    public function removeCare(Care $care): self
    {
        if ($this->cares->removeElement($care)) {
            // set the owning side to null (unless already changed)
            if ($care->getOffice() === $this) {
                $care->setOffice(null);
            }
        }

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        if ($creator instanceof User) {
            $this->addPro($creator);
        }

        return $this;
    }
}
