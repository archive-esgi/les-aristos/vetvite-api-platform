<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\User;
use App\Repository\AppointmentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AppointmentRepository::class)
 * @ApiResource(
 *     mercure=true,
 *     attributes={"order"={"date": "DESC"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_USER')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object.getClient() == user or object.getCare().getPro() == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object.getClient() == user or object.getCare().getPro() == user"}
 *     },
 *     normalizationContext={"groups"={"appointment:read"}, "enable_max_depth"="true"},
 *     denormalizationContext={"groups"={"appointment:write"}, "enable_max_depth"="true"}
 * )
 * @ApiFilter(DateFilter::class, properties={"date", "createdAt"})
 * @ApiFilter(SearchFilter::class, properties={"client": "exact", "care.pro": "exact"})
 */
class Appointment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"appointment:read", "office:read","care:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetimetz")
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"appointment:read", "appointment:write","office:read","care:read"})
     */
    private $date;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"appointment:read", "appointment:write","office:read","care:read"})
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"appointment:read", "appointment:write","office:read"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Care::class, inversedBy="appointments")
     * @Groups({"appointment:read", "appointment:write"})
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(2)
     */
    private $care;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="appointments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"appointment:read", "appointment:write","office:read","care:read"})
     * @MaxDepth(1)
     */
    private $client;

    /**
     * @ORM\OneToOne(targetEntity=Appointment::class, cascade={"persist", "remove"})
     * @Groups({"appointment:read", "appointment:write","office:read","care:read"})
     * @MaxDepth(1)
     */
    private $related;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCare(): ?Care
    {
        return $this->care;
    }

    public function setCare(?Care $care): self
    {
        $this->care = $care;

        return $this;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getRelated(): ?self
    {
        return $this->related;
    }

    public function setRelated(?self $related): self
    {
        $this->related = $related;

        return $this;
    }
}
