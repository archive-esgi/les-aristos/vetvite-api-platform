<?php

namespace App\Security;

use App\Entity\User as AppUser;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if ($user->getEnabled() === AppUser::SET_BANNED) {
            throw new CustomUserMessageAccountStatusException("account.banned");
        } elseif ($user->getEnabled() === AppUser::SET_DISABLED) {
            throw new CustomUserMessageAuthenticationException('account.notfound');
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
    }
}
