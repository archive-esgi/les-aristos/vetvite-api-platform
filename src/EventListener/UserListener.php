<?php
namespace App\EventListener;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Service\Mailer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserListener implements EventSubscriber
{

    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;

    /**
     * @var Mailer
     */
    private $mailer;

    public function __construct(UserPasswordEncoder $passwordEncoder, Mailer  $mailer)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    //

    public function preUpdate(LifecycleEventArgs $event)
    {
        $this->prePersist($event);
    }

    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!$entity instanceof User) {
            return;
        }

        $this->encodePassword($entity);
        $entity->eraseCredentials();

        $type = ($entity->type) ? strtolower($entity->type) : '' ;

        if ($type === 'pro') {
            $entity->setUserHasPro(true);
        } elseif ($type === 'client') {
            $entity->setUserHasClient(true);
        } elseif ($type === 'admin') {
            return;
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof User) {
            return;
        }

        $this->checkPasswordUpdate($entity);

        $changes = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity);
        if (array_key_exists('email', $changes)) {
            list($old, $new) = $changes['email'];
            $this->mailer->sendEmailChanged($entity, $old, $new);
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof User) {
            return;
        }

        if ($entity->getEnabled() === User::SET_WAITING) {
            $this->mailer->sendConfirmationEmail($entity);
        }

        $entity->eraseCredentials();
    }

    private function encodePassword(User $entity)
    {
        if (!$entity->getPlainPassword()) {
            return;
        }

        if (strlen($entity->getPlainPassword()) === 0) {
            return;
        }

        $encoded = $this->passwordEncoder->encodePassword(
            $entity,
            $entity->getPlainPassword()
        );

        $entity->setPassword($encoded);
    }

    private function checkPasswordUpdate(User $entity)
    {
        if (strlen($entity->getPlainPassword()) === 0) {
            return;
        }

        $this->mailer->sendPasswordChanged($entity, $entity->getPlainPassword());

        $entity->eraseCredentials();
    }
}
