<?php

namespace App\Command;

use App\Entity\User\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserAdminCreateCommand extends Command
{
    protected static $defaultName = 'user:create:admin';
    private $encoder;
    private $em;

    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em, string $name = null)
    {
        parent::__construct($name);
        $this->encoder = $encoder;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create a new admin user')
            ->addArgument('email', InputArgument::REQUIRED, 'Email of user')
            ->addArgument('password', InputArgument::REQUIRED, 'Password of user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');

        $user = new Admin();
        $user->setEmail($email)
            ->setPassword($this->encoder->encodePassword($user, $password))
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(1)
            ->setFirstname("Admin")
            ->setLastName("Admin")
            ->setDob(new \DateTime());
        $this->em->persist($user);
        $this->em->flush();

        $io->success('User created.');

        return Command::SUCCESS;
    }
}
