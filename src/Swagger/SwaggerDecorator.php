<?php

declare(strict_types=1);

namespace App\Swagger;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class SwaggerDecorator implements NormalizerInterface
{
    /** @var NormalizerInterface */
    private $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        $tokenDocumentation = [
            'paths' => [
                '/api/login' => [
                    'post' => [
                        'tags' => ['Other'],
                        'operationId' => 'postCredentialsItem',
                        'summary' => 'Get JWT token to login.',
                        'requestBody' => [
                            'description' => 'Create new JWT Token',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'email' => [
                                                'type' => 'string',
                                                'example' => 'admin@example.com',
                                            ],
                                            'password' => [
                                                'type' => 'string',
                                                'example' => 'motdepasse',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Get JWT token',
                                'content' => [
                                    'application/json' => [
                                        'schema' => [
                                            'type' => 'object',
                                            'properties' => [
                                                'token' => [
                                                    'type' => 'string',
                                                    'readOnly' => true,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                '/api/user/forgot' => [
                    'post' => [
                        'tags' => ['Other'],
                        'summary' => 'Forgot Password',
                        'requestBody' => [
                            'description' => 'Send email to user for reset password',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'email' => [
                                                'type' => 'string',
                                                'example' => 'example@vetvite.fr',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Email sent',
                            ],
                            Response::HTTP_BAD_REQUEST => [
                                'description' => 'Email not sent',
                            ],
                        ],
                    ],
                ],
                '/api/user/reset' => [
                    'post' => [
                        'tags' => ['Other'],
                        'summary' => 'Reset Password',
                        'requestBody' => [
                            'description' => 'Reset / Change the user password',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'token' => [
                                                'type' => 'string',
                                                'example' => 'a1z2e3r4t56Y7U8I9O0P',
                                            ],
                                            'plainPassword' => [
                                                'type' => 'string',
                                                'example' => 'Motdepasse123@',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Password updated',
                            ],
                            Response::HTTP_BAD_REQUEST => [
                                'description' => 'Password not updated',
                            ],
                        ],
                    ],
                ],
                '/api/user/confirm' => [
                    'post' => [
                        'tags' => ['Other'],
                        'summary' => 'Confirm user account',
                        'requestBody' => [
                            'description' => 'Confirm user account',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'code' => [
                                                'type' => 'string',
                                                'example' => 'A123Z',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Account verified',
                            ],
                            Response::HTTP_BAD_REQUEST => [
                                'description' => 'Account not verified',
                            ],
                        ],
                    ],
                ],
                '/api/user/sendconfirm' => [
                    'post' => [
                        'tags' => ['Other'],
                        'summary' => 'send confirm user account email',
                        'requestBody' => [
                            'description' => 'Send confirm user account email',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'email' => [
                                                'type' => 'string',
                                                'example' => 'example@vetvite.fr',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Email send',
                            ],
                            Response::HTTP_BAD_REQUEST => [
                                'description' => 'Email not sent',
                            ],
                        ],
                    ],
                ],
                '/api/user/gdpr/download' => [
                    'post' => [
                        'tags' => ['Other'],
                        'summary' => 'GDPR data download request',
                        'requestBody' => [
                            'description' => 'Send current user GDPR data download request to admin',
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Request sent',
                            ],
                            Response::HTTP_BAD_REQUEST => [
                                'description' => 'Request not sent',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return array_merge_recursive($docs, $tokenDocumentation);
    }
}
