<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Doctrine\ORM\QueryBuilder;

class FullTextByWordsSearchFilter extends SearchFilter
{
    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if ($property !== 'search' || !$value) {
            return;
        }

        $words = explode(' ', $value);

        $andExpressions = [];
        
        foreach ($words as $index => $word) {
            foreach ($this->properties as $propertyName => $strategy) {
                $strategy = $strategy ?? self::STRATEGY_EXACT;
                $alias = $queryBuilder->getRootAliases()[0];
                $field = $propertyName;

                // check if the class has the property
                $associations = [];
                if ($this->isPropertyNested($propertyName, $resourceClass)) {
                    [$alias, $field, $associations] = $this->addJoinsForNestedProperty($propertyName, $alias, $queryBuilder, $queryNameGenerator, $resourceClass);
                }
                $metadata = $this->getNestedMetadata($resourceClass, $associations);

                if ($metadata->hasField($field)) {
                    //the property exist in the class
                    
                    if ('id' === $field) {
                        $word = $this->getIdFromValue($word);
                    }
                    
                    // check if the word is the same type as the ORM property
                    if (!$this->hasValidValues((array)$word, $this->getDoctrineFieldType($propertyName, $resourceClass))) {
                        continue;
                    }

                    $andExpressions[] = $this->addWhereByStrategy($strategy, $queryBuilder, $queryNameGenerator, $alias, $field, $word);
                }
            }
            $queryBuilder->andWhere($queryBuilder->expr()->orX(...$andExpressions));

            $andExpressions = [];
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function addWhereByStrategy(string $strategy, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, $word, $sensitiveCase = false)
    {
        $wrapCase = $this->createWrapCase($sensitiveCase);
        $wordParameter = $queryNameGenerator->generateParameterName($field);
        $exprBuilder = $queryBuilder->expr();

        $queryBuilder->setParameter($wordParameter, $word);

        switch ($strategy) {
            case null:
            case self::STRATEGY_EXACT: case '=':
                return $exprBuilder->eq($wrapCase("$alias.$field"), $wrapCase(":$wordParameter"));
            case self::STRATEGY_PARTIAL: case '%':
                return $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat("'%'", $wrapCase(":$wordParameter"), "'%'"));
            case self::STRATEGY_START:
                return $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat($wrapCase(":$wordParameter"), "'%'"));
            case self::STRATEGY_END:
                return $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat("'%'", $wrapCase(":$wordParameter")));
            case self::STRATEGY_WORD_START: case 'startOrEnd':
                return $exprBuilder->orX(
                    $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat($wrapCase(":$wordParameter"), "'%'")),
                    $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat("'%'", $wrapCase(":$wordParameter")))
                );
            default:
                throw new InvalidArgumentException(sprintf('strategy %s does not exist.', $strategy));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        $descriptions = [];
        $propertyNames = [];

        foreach ($this->properties as $property => $strategy) {
            if (!$this->isPropertyMapped($property, $resourceClass, true)) {
                continue;
            }

            $propertyNames[] = $this->normalizePropertyName($property);
        }

        $descriptions['search'] = [
            'property' => 'search',
            'type' => 'string',
            'required' => false,
            'is_collection' => false,
            'openapi' => [
                'description' => 'Search in fields: ' . implode(', ', $propertyNames),
            ],
        ];

        return $descriptions;
    }
}
