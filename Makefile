run-local:
	docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d

run-dev:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d

run-prod:
	docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

stop:
	docker-compose down

install:
	make reset-db

reset-db:
	make console CMD="doctrine:schema:drop --force"
	make console CMD="doctrine:schema:create"
	make console CMD="hautelook:fixtures:load --no-interaction"

console:
	docker-compose exec php-fpm php bin/console $(CMD)

update:
	make console CMD="doctrine:schema:update --force"

generate:
	mkdir -p config/jwt
	docker-compose exec php-fpm openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
	docker-compose exec php-fpm openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
	docker-compose exec php-fpm chmod 775 -R config/jwt